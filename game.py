# from file random take function "randint"
from random import randint
# prompt user to give their name
name = input("Hi! What is your name?")
# define function


def guess_my_birthday():
    # declare month and year variables with randint for months and provided year range
    month = randint(1, 12)
    year = randint(1924, 2004)
    # create flag that keeps track of whether correct date was guessed
    guessed_correctly = "false"
    # loop 5 times
    for num in range(5):
        # print message of guess and request input from user
        print("Guess", num + 1, ":", name,
              "were you born in", month, "/", year, "? ")
        response = input("yes or no?")
        # if lowercase response is equal to "yes" lower case...
        if response.lower() == "yes".lower():
            # print message and change guessed_correctly flag to true
            print("I knew it!")
            guessed_correctly = "true"
            # exit loop
            break
        # else if num is equal to 4 (the last iteration of range(5)) exit loop
        elif num == 4:
            break
        # otherwise print message
        else:
            print("Drat! Lemme try again!")
    # if after loop, guessed correctly is still false, print message
    if guessed_correctly == "false":
        print("I have other things to do. Good bye.")


# call function
guess_my_birthday()
